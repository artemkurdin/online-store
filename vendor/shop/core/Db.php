<?php


namespace shop;


class Db
{
    use TSingletone;

    private function __construct()
    {
        $db = require_once CONFIG . '/config_db.php';
        class_alias('\RedBeanPHP\R', 'R');
        \R::setup($db['dsn'], $db['user'], $db['password']);
        if( !\R::testConnection() ){
            throw new \Exception('Ошибка соединения с БД', 500);
        }

        \R::freeze(true);
        if( DEBUG ){
            \R::debug(true, 1);
        }
    }
}