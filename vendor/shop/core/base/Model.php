<?php


namespace shop\base;

use shop\Db;
use Valitron\Validator;

abstract class Model
{
    public $attributes = [];
    public $errors;
    public $rules;

    public function __construct()
    {
        Db::instance();
    }

    public function load($data)
    {
        foreach ($this->attributes as $k => $v){
            if( isset($data[$k]) ){
                $this->attributes[$k] = $data[$k];
            }
        }
    }

    // Сохраняет пользваотеля $table = таблица для сохранения
    public function save($table)
    {
        $tbl = \R::dispense($table);
        foreach($this->attributes as $name => $value){
            $tbl->$name = $value;
        }
        return \R::store($tbl);
    }

    public function validate($data)
    {
        Validator::langDir(WWW . '/validator/lang');
        Validator::lang('ru');
        $v = new Validator($data);
        $v->rules($this->rules);
        if($v->validate()){
            return true;
        }
        $this->errors = $v->errors();
        return false;
    }

    public function getErrors()
    {
        $errors = "<ul>";
        foreach($this->errors as $error){
            foreach ($error as $item){
                $errors .= "<li>$item</li>";
            }
        }
        $errors .= "</ul>";
        $_SESSION['errors'] = $errors;
    }
}