<?php


namespace shop\base;


abstract class Controller
{
    public $route;
    public $controller;
    public $view;
    public $model;
    public $layout;
    public $prefix;
    public $data;
    public $meta = ['title' => '', 'description' => '', 'keywords' => ''];

    public function __construct($route)
    {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->model = $route['controller'];
        $this->view = $route['action'];
        $this->prefix = $route['prefix'];
    }

    public function getView()
    {
        $viewObject = new View($this->route, $this->layout, $this->view, $this->meta);
        $viewObject->render($this->data);
    }

    public function set($data)
    {
        $this->data = $data;
    }

    public function setMeta($title = '', $description = '', $keywords = '')
    {
        $this->meta['title'] = $title;
        $this->meta['description'] = $description;
        $this->meta['keywords'] = $keywords;
        return $this->meta;
    }

    public function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

    // возвращает html-ответ на AJAX-запрос
    // Параметры: вид для подключения и массив возможных данных для использования в виде
    public function loadView($view, $vars = [])
    {
        extract($vars);
        require APP . "/views/{$this->prefix}{$this->controller}/$view.php";
        die;
    }

    public function exceptionError($textError = 'Страница не найдена', $errorCode = 404)
    {
        throw new \Exception($textError, $errorCode);
        die;
    }

}