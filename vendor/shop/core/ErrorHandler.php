<?php

namespace shop;

class ErrorHandler{

    public function __construct()
    {
        if(DEBUG){
            error_reporting(-1);
        }else{
            error_reporting(0);
        }

        set_exception_handler([$this, 'exceptionHandler']);

        if( QUARANTINE ){
            require WWW . '/errors/quarantine.php';
            die;
        }
    }

    public function exceptionHandler($e)
    {
        $this->logErrors($e->getMessage(), $e->getFile(), $e->getLine());
        $this->displayError('Исключение', $e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode());
    }

    protected function logErrors($message = '', $file = '', $line = '')
    {
        error_log("[" . date('Y-m-d H:i:s') . "] Текст ошибки: {$message} | Файл: {$file} | Строка: {$line}\n\n*************************************\n\n", 3, ROOT . '/tmp/errors.txt');
    }

    protected function displayError($errno, $errstr, $errfile, $errline, $responce = 404)
    {
        http_response_code($responce);

        if( $responce == 404 && !DEBUG ){
            require WWW . '/errors/404.php';
            die;
        } else if( $responce == 500 && !DEBUG ){
            require WWW . '/errors/500.php';
            die;
        } else if( $responce == 503 && !DEBUG ){
            require WWW . '/errors/503.php';
            die;
        }

        if(DEBUG){
            require WWW . '/errors/dev.php';
        }else{
            require WWW . '/errors/prod.php';
        }
        die;
    }

}